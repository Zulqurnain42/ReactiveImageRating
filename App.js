/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/
import React, { Component } from 'react';
import { Root } from './app/config/router';
import * as firebase from 'firebase';

export default class App extends Component {
	jsonData;

	constructor(props) {
        super(props);
        this.state = {
            listDataFromChild: null
        };    
	}
	
    myCallback = (dataFromChild) => {
        this.setState({ listDataFromChild: dataFromChild });
	}
	
	componentWillMount() {
		
	    const firebaseConfig = {
			apiKey: "AIzaSyDu9I18SOwWpp0fGcT7uuk9WsVVL42oTqw",
			authDomain: "reactnativepoll.firebaseapp.com",
			databaseURL: "https://reactnativepoll.firebaseio.com",
			projectId: "reactnativepoll",
			storageBucket: "reactnativepoll.appspot.com",
			messagingSenderId: "827346196261"
		};

		firebase.initializeApp(firebaseConfig);
		
		firebase.database().ref('users').on('value', (data) => {
			this.jsonData = data.toJSON();
		})

		// setTimeout(() => {
		// 	firebase.database().ref('users/003').set(
		// 		{
		// 			"category_id" : "3",
		// 			"created_at" : "2017-12-06T15:40:49.499Z",
		// 			"description" : "What do you think ?",
		// 			"detail" : {
		// 				"category" : {
		// 				"created_at" : "2018-05-01T11:34:49.415Z",
		// 				"description" : "A photo showing the impact of polution on environment",
		// 				"id" : 2,
		// 				"title" : "Science",
		// 				"updated_at" : "2018-05-01T11:34:49.415Z"
		// 				},
		// 				"category_id" : 3,
		// 				"created_at" : "2018-04-30T15:40:49.499Z",
		// 				"description" : "What do you think?",
		// 				"end_date" : "2018-04-29T00:00:00.000Z",
		// 				"id" : 3,
		// 				"picture" : {
		// 				"created_at" : "2018-05-01T11:34:49.415Z",
		// 				"id" : 3,
		// 				"imageable_id" : 3,
		// 				"imageable_type" : "Sondage",
		// 				"path" : "http://res.cloudinary.com/sondage-ma/image/upload/v1512574850/rm2kevxbths2sm1y3dib.jpg",
		// 				"updated_at" : "2018-05-01T11:34:49.415Z"
		// 				},
		// 				"rating" : {
		// 				"rating" : 3
		// 				},
		// 				"start_date" : "2018-04-06T00:00:00.000Z",
		// 				"title" : "Impact of poullution on ecosystem",
		// 				"type" : {
		// 				"created_at" : "2018-05-01T11:34:49.415Z",
		// 				"id" : 2,
		// 				"title" : "IN OUT",
		// 				"updated_at" : "2018-05-01T11:34:49.415Z"
		// 				},
		// 				"type_id" : 1,
		// 				"updated_at" : "2018-05-01T15:40:49.499Z",
		// 				"user" : {
		// 				"email" : "default@example.com",
		// 				"username" : "redaction"
		// 				},
		// 				"user_id" : 2
		// 			},
		// 			"end_date" : "2017-12-31T00:00:00.000Z",
		// 			"id" : "6",
		// 			"picture" : {
		// 				"created_at" : "2017-12-06T15:40:50.427Z",
		// 				"id" : 3,
		// 				"imageable_id" : "3",
		// 				"imageable_type" : "Sondage",
		// 				"path" : "https://www.the-scientist.com/images/August2015/feature1_infograph.jpg",
		// 				"updated_at" : "2017-12-06T15:40:50.427Z"
		// 			},
		// 			"rating" : 9,
		// 			"start_date" : "2017-12-06T00:00:00.000Z",
		// 			"title" : "Should there be more plants and greenery in the environment to curb pollution?",
		// 			"type_id" : "1",
		// 			"updated_at" : "2017-12-06T15:40:49.499Z",
		// 			"user_id" : "1"
		// 			}

		// 	).then(() => {
		// 		//console.log('INSERTED !');

		// 	}).catch((error) => {
		// 		console.log(error);
		// 	});
		// }, 5000);
		// firebase.database().ref('users/001').update({
		// 	name: 'Pheng Sengvuthy'
		// });
	}

	render() {
		return <Root />;
	}
}
